#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from scipy.special import digamma, polygamma, loggamma
from scipy import optimize

def inv(x):

    eigvals, eigvecs = np.linalg.eigh(x)
    eigvals = eigvals ** (-1)
    eigvals = np.vectorize(np.diag, signature='(n)->(n,n)')(eigvals)
    transp_eigvecs = np.transpose(eigvecs, axes=(0, 2, 1))
    result = np.matmul(eigvecs, eigvals)
    result = np.matmul(result, transp_eigvecs)

    return result

class RobustFusionProbabilityMaps():

    def __init__(self, config):

        self.n_iter = config["n_iter"]
        self.eps = config["eps"]
        self.config = config
        self.tol = config["tol"]
        self.nr_n_iter = config["nr_n_iter"]
        self.nr_tol = config["nr_tol"]

    def initialization(self, D):
        '''
        :param D: N x P x K
        :return:
        '''

        self.N, self.P, self.K = D.shape

        mu_b = np.zeros((self.P, self.K))
        precision_p = np.stack([np.eye(self.K)*1e2 for i in range(self.P)])
        nu = np.ones(self.P)
        tau = np.random.randn(self.N, self.P) ** 2
        beta = np.random.random()
        gamma_0 = 1e-3
        S_0 = np.eye(self.K) * 1e3

        return mu_b, precision_p, nu, tau, beta, gamma_0, S_0

    def q_Tn(self, tau, precision_p, D, mu_b):
        '''
        :param tau: N x P
        :param precision_p: P x K x K
        :param D: N x P x K
        :param mu_b: P x K
        :return: sigma_Tn, mu_Tn
        '''

        sigma_Tn = np.einsum('np,pkl->nkl', tau, precision_p) # N x K x K
        sigma_Tn = inv(sigma_Tn) # N x K x K

        D_b = D - mu_b #  N x P x K
        precision_p_D_b = np.einsum('pkl,npl->npk', precision_p, D_b) # N x P x K
        tau_precision_p_D_b = np.einsum('np,npk->nk', tau, precision_p_D_b) # N x K
        mu_Tn = np.einsum('nkl,nl->nk', sigma_Tn, tau_precision_p_D_b) # N x K

        return mu_Tn, sigma_Tn

    def q_tau(self, nu, mu_Tn, sigma_Tn, precision_p, D, mu_b, sigma_b):
        '''
        :param nu: P
        :param mu_Tn: N x K
        :param sigma_Tn: N x K x K
        :param precision_p: P x K x K
        :param D: N x P x K
        :param mu_b: P x K
        :param sigma_b: P x K x K
        :return: tau, a, b
        '''

        a = np.repeat(np.reshape((nu + self.K) / 2, (1, -1)), self.N, axis=0) # N * P

        D_mu_Tn = np.transpose(D, (1,0,2)) - mu_Tn # P x N x K
        D_mu_Tn_mu_b = np.transpose(D_mu_Tn, (1,0,2)) - mu_b # N x P x K

        maha = np.einsum('npk,pkl,npl->np', D_mu_Tn_mu_b, precision_p, D_mu_Tn_mu_b) # N x P

        precision_p_sigma_b = np.einsum('pkl,plj->pkj', precision_p, sigma_b) # P x K x K
        precision_p_sigma_Tn = np.einsum('pkl,nlj->npkj', precision_p, sigma_Tn) # N x P x K x K

        b = np.repeat(np.reshape(nu / 2, (1, -1)), self.N, axis=0) + \
            1/2 * (maha + np.trace(precision_p_sigma_b, axis1=1, axis2=2) + \
            np.trace(precision_p_sigma_Tn, axis1=2, axis2=3)) # N x P

        tau = a/b

        return tau, a, b

    def q_b(self, tau, precision_p, beta, D, mu_Tn):
        '''
        :param tau: N x P
        :param precision_p: P x K x K
        :param beta: scalar
        :param D: N x P x K
        :param mu_Tn: N x K
        :return: mu_b, sigma_b
        '''

        tau_sum = np.sum(tau, axis=0) # P
        sigma_b = np.einsum('p,pkl->pkl', tau_sum, precision_p) + beta*np.eye(self.K, self.K) # P x K x K
        sigma_b = inv(sigma_b)

        D_Tn = np.transpose(D, (1,0,2)) - mu_Tn # P x N x K
        D_Tn = np.transpose(D_Tn, (1,0,2)) # N x P x K
        tau_D_Tn = np.einsum('np,npk->pk', tau, D_Tn) # P x K
        precision_p_tau_D_Tn = np.einsum('pkl,pl->pk', precision_p, tau_D_Tn) # P x K
        mu_b = np.einsum('pkl,pl->pk', sigma_b, precision_p_tau_D_Tn)  # P x K

        return mu_b, sigma_b

    def q_beta(self, sigma_b, mu_b):
        '''
        :param sigma_b: P x K x K
        :param mu_b: P x K
        :return: beta
        '''

        beta = self.P * self.K / np.sum(np.trace(sigma_b, axis1=1, axis2=2) + np.sum(mu_b**2, axis=1))

        return beta

    def q_precision_p(self, D, mu_Tn, mu_b, sigma_Tn, sigma_b, tau, gamma_0, S_0):
        '''
        :param D: N x P x K
        :param mu_Tn: N x K
        :param mu_b: P x K
        :param sigma_Tn: N x K x K
        :param sigma_b: P x K x K
        :param tau: N x P
        :param gamma0: scalar
        :param S0: K x K
        :return: precision_p, sigma_p
        '''

        gamma_p = np.ones(self.P) * (self.N + gamma_0) # P

        D_mu_Tn = np.transpose(D, (1, 0, 2)) - mu_Tn  # P x N x K
        D_mu_Tn_mu_b = np.transpose(D_mu_Tn, (1, 0, 2)) - mu_b  # N x P x K

        m = np.einsum('npk,np,npl->pkl', D_mu_Tn_mu_b, tau, D_mu_Tn_mu_b)  # P x K x K
        tau_sigma_Tn = np.einsum('np,nkl->pkl', tau, sigma_Tn) # P x K x K
        tau_sigma_b = np.einsum('np,pkl->pkl', tau, sigma_b)  # P x K x K
        S_p = inv(S_0[np.newaxis, ...])[0] + m + tau_sigma_b + tau_sigma_Tn # P x K x K
        S_p = inv(S_p)

        precision_p = np.einsum('p,pkl->pkl', gamma_p, S_p) # P x K x K

        sigma_p = inv(precision_p)

        return S_p, gamma_p, precision_p, sigma_p

    def q_nu(self, tau, a, nu):
        '''
        :param tau: N x P
        :param a: N x P
        :param nu: P
        :return: nu
        '''

        # Calculate the constant part of the equation to calculate the
        # new degrees of freedom
        constant_part = 1.0 \
                        + (np.log(tau) - tau)  \
                        + digamma(a) \
                        - np.log(a)
        constant_part = np.sum(constant_part, axis=0) / self.N  # Summation over N

        # Solve the equation numerically using Newton-Raphson
        nu_new = np.empty_like(nu)
        for c in range(self.P):
            def func(x):
                return np.log(x / 2.0) \
                       - digamma(x / 2.0) \
                       + constant_part[c]

            def fprime(x):
                return 1.0 / x \
                       - polygamma(1, x / 2.0) / 2.0

            def fprime2(x):
                return - 1.0 / (x * x) \
                       - polygamma(2, x / 2.0) / 4.0

            nu_new[c] = optimize.newton(
                func, nu[c], fprime, args=(), tol=self.nr_tol,
                maxiter=self.nr_n_iter, fprime2=fprime2
            )
            if nu_new[c] < 0.0:
                raise ValueError('[q_nu] Error, degree of freedom smaller than one.')

        return nu_new

    def lower_bound(self, tau, a, b, precision_p, gamma_p, S_p, gamma_0, S_0, D, mu_Tn, mu_b,
                    sigma_Tn, sigma_b, beta, nu):
        '''
        :param tau: N x P
        :param a: N x P
        :param b: N x P
        :param precision_p: P x K x K
        :param gamma_p: P
        :param S_p: P x K x K
        :param gamma_0: scalar
        :param S_0: K x K
        :param D: N x P x K
        :param mu_Tn: N x K
        :param mu_b: P x K
        :param sigma_Tn: N x K x K
        :param sigma_b: P x K x K
        :param beta: scalar
        :param nu: P
        :return: lower bound
        '''

        _, log_det_precision_p = np.linalg.slogdet(precision_p)
        _, log_det_sigma_Tn = np.linalg.slogdet(sigma_Tn)
        _, log_det_sigma_b = np.linalg.slogdet(sigma_b)
        _, log_det_S_p = np.linalg.slogdet(S_p)

        D_mu_Tn = np.transpose(D, (1, 0, 2)) - mu_Tn  # P x N x K
        D_mu_Tn_mu_b = np.transpose(D_mu_Tn, (1, 0, 2)) - mu_b  # N x P x K
        maha = np.einsum('npk,pkl,np,npl->np', D_mu_Tn_mu_b, precision_p, tau, D_mu_Tn_mu_b)  # N x P

        precision_p_sigma_b = np.einsum('pkl,plj->pkj', precision_p, sigma_b)  # P x K x K
        precision_p_sigma_Tn = np.einsum('pkl,nlj->npkj', precision_p, sigma_Tn)  # N x P x K x K
        traces = tau * (np.trace(precision_p_sigma_b, axis1=1, axis2=2) + np.trace(precision_p_sigma_Tn, axis1=2, axis2=3)) # N x P

        p_D = self.K/2 * (digamma(a) - np.log(b)) + 1/2 * log_det_precision_p - 1/2 * maha - 1/2 * traces # N x P
        p_D = np.sum(p_D)

        p_b = self.K/2 * np.log(beta) - 1/2 * beta * np.sum(mu_b**2, axis=1) # P
        p_b = np.sum(p_b)

        p_tau = - loggamma(nu/2) + nu/2*np.log(nu/2) + (nu/2-1)*(digamma(a) - np.log(b)) - nu/2*tau # N x P
        p_tau = np.sum(p_tau)

        sum_digamma = np.sum(np.stack([digamma((gamma_p + 1 - i)/2) for i in range(self.K)]), axis=0) # P
        S_0_inv = inv(S_0[np.newaxis, ...])[0] # K x K
        p_precision_p = (gamma_0 - self.K -1)/2 * (sum_digamma + log_det_S_p) - 1/2 * np.trace(np.einsum('kl,plj->pkj', S_0_inv, S_p), axis1=1, axis2=2) # P
        p_precision_p = np.sum(p_precision_p)

        q_Tn = - 1/2 * log_det_sigma_Tn  # N
        q_Tn = np.sum(q_Tn)

        q_b = -1/2 * log_det_sigma_b  # P
        q_b = np.sum(q_b)

        q_tau = - loggamma(a) + np.log(b) + (a-1)*digamma(a) - a # N x P
        q_tau = np.sum(q_tau)

        sum_gamma = np.sum(np.stack([loggamma((gamma_p + 1 - i)/2) for i in range(self.K)]), axis=0) # P
        B = -gamma_p/2 * log_det_S_p - gamma_p*self.K/2 * np.log(2) - sum_gamma # P
        q_precision_p = B + (gamma_p - self.K -1)/2 * (sum_digamma + log_det_S_p) - self.K/2 * gamma_p # P
        q_precision_p = np.sum(q_precision_p)

        lb = p_D + p_tau + p_b + p_precision_p - q_tau - q_b - q_Tn - q_precision_p

        return lb


    def fit(self, D):
        '''
        :param D: N x P x K
        '''

        mu_b, precision_p, nu, tau, beta, gamma_0, S_0 = self.initialization(D)

        lower_bound_list = []
        converged = 0
        i = 0
        while i < self.n_iter:

            mu_Tn, sigma_Tn = self.q_Tn(tau, precision_p, D, mu_b)

            mu_b, sigma_b = self.q_b(tau, precision_p, beta, D, mu_Tn)

            tau, a, b = self.q_tau(nu, mu_Tn, sigma_Tn, precision_p, D, mu_b, sigma_b)

            beta = self.q_beta(sigma_b, mu_b)

            S_p, gamma_p, precision_p, sigma_p = self.q_precision_p(D, mu_Tn, mu_b, sigma_Tn, sigma_b, tau, gamma_0, S_0)

            nu = self.q_nu(tau, a, nu)

            lb = self.lower_bound(tau, a, b, precision_p, gamma_p, S_p, gamma_0, S_0, D, mu_Tn, mu_b,
                                  sigma_Tn, sigma_b, beta, nu)

            if i > 1:
                # We check that the lower bounds is increasing
                assert lb >= lower_bound_list[-1]

            lower_bound_list.append(lb)
            if i > 1 and lower_bound_list[-1] - lower_bound_list[-2] < self.tol:
                i = self.n_iter
                converged = 1

            i += 1

        # Check convergence
        if not converged and self.n_iter > 0:
            print("Algorithm did not converged")
        else:
            print("Algorithm converged")
        print()


        self.lower_bound_list = lower_bound_list
        self.mu_Tn = mu_Tn
        self.sigma_Tn = sigma_Tn
        self.mu_b = mu_b
        self.sigma_b = sigma_b
        self.nu = nu
        self.precision_p = precision_p
        self.sigma_p = sigma_p
        self.tau = tau
        self.beta = beta




if __name__ == "__main__":

    from scipy.ndimage.morphology import distance_transform_edt
    from scipy.special import expit
    import matplotlib.pyplot as plt
    from math import *


    def multivariate_t_rvs(m, S, df=np.inf, n=1):
        '''generate random variables of multivariate t distribution
        Parameters
        ----------
        m : array_like
            mean of random variable, length determines dimension of random variable
        S : array_like
            square array of covariance  matrix
        df : int or float
            degrees of freedom
        n : int
            number of observations, return random array will be (n, len(m))
        Returns
        -------
        rvs : ndarray, (n, len(m))
            each row is an independent draw of a multivariate t distributed
            random variable
        '''
        m = np.asarray(m)
        d = len(m)
        if df == np.inf:
            x = 1.
        else:
            x = np.random.chisquare(df, n) / df
        z = np.random.multivariate_normal(np.zeros(d), S, (n,))
        return m + z / np.sqrt(x)[:, None]


    config = {
        "tol": 1e-1,
        "nr_tol": 1e-6,
        "n_iter": 200,
        "nr_n_iter": 1000,
        "eps": 1e-8
    }

    # Draw an image with a square at the center and compute a distance map then
    # to be converted into a probability map
    d = 30
    N = d**2
    b = 5
    img = np.zeros((d,d))
    img[max(0,int(d/2)-b):min(int(d/2)+b,d), max(0,int(d/2)-b):min(int(d/2)+b,d)] = 1
    dm1 = distance_transform_edt(img)
    dm2 = distance_transform_edt(np.abs(img-1))
    dm = dm1 - dm2
    dm = expit(dm)

    plt.figure()
    plt.imshow(dm)
    plt.clim((0,1))
    plt.title("Distance map", size=13)
    plt.axis("off")
    plt.show()

    # Numbers
    K = 2 # Background and foreground
    P = 3 # Number of raters

    # True values
    nu = np.array([500, 2.4, 5.3])
    b = np.array([[0.1, 0.05],  # p x K
                  [-0.1, 0.001],
                  [0.06, -0.1]])
    Tn = np.reshape(dm, (-1,1))
    Tn = np.concatenate([Tn, np.abs(1-Tn)], axis=1)
    Tn = np.sqrt(Tn) # N x K

    # Plot of true background and foreground
    label = ["Foreground", "Background"]
    count = 0
    plt.figure(figsize=[K * 5, 5])
    for j in range(K):
        tmp = Tn[:, j]
        tmp = np.reshape(tmp, img.shape)
        count += 1
        plt.subplot(1, K, count)
        plt.imshow(tmp)
        plt.clim((0, 1))
        plt.axis("off")
        plt.title(label[j], size=13)
    plt.tight_layout()
    plt.show()

    # Setting for the rater maps
    sigma_p = np.stack([np.eye(K)*0.002,
                        np.eye(K)*0.005,
                        np.eye(K)*0.01]) # P x K x K

    D = np.empty((N, P, K))
    for p in range(P):
        for i in range(N):
            sig = sigma_p[p]
            bias = b[p,:]
            tn = Tn[i,:]
            D[i,p,:] = multivariate_t_rvs(tn+bias, sig, df=nu[p], n=1)

    # Plot of rater maps
    count = 0
    plt.figure(figsize=[K * 5, 5 * P])
    for i in range(P):
        for j in range(K):
            tmp = D[:,i,j]
            tmp = np.reshape(tmp, img.shape)
            count += 1
            plt.subplot(P,K,count)
            plt.imshow(tmp)
            plt.clim((0,1))
            plt.axis("off")
            plt.title("Rater "+str(i+1), size=13)
    plt.tight_layout()
    plt.show()

    fusion_model = RobustFusionProbabilityMaps(config)

    fusion_model.fit(D)

    plt.figure()
    plt.plot(fusion_model.lower_bound_list)
    plt.title("Lower bound", size=13)
    plt.xlabel("Iterations", size=9)
    plt.tight_layout()
    plt.show()


    print("True bias:")
    print(b)
    print()
    print("Estimated bias:")
    print(fusion_model.mu_b)
    print()

    print("True degrees of freedom:")
    print(nu)
    print()
    print("Estimated degrees of freedom:")
    print(fusion_model.nu)
    print()

    print("True sigma_p:")
    for i in range(P):
        print(sigma_p[i])
    print()
    print("Estimated sigma_p:")
    for i in range(P):
        print(fusion_model.sigma_p[i])
    print()

    # Plot of estimated consensus
    consensus = fusion_model.mu_Tn
    count = 0
    plt.figure(figsize=[5*2,K*5])
    for k in range(K):
        tmp = Tn[:,k]
        tmp = np.reshape(tmp, img.shape)
        count += 1
        plt.subplot(K,2,count)
        plt.imshow(tmp)
        plt.clim((0,1))
        plt.axis("off")
        plt.title("True map", size=13)
        tmp = consensus[:,k]
        tmp = np.reshape(tmp, img.shape)
        count += 1
        plt.subplot(K,2,count)
        plt.imshow(tmp)
        plt.clim((0,1))
        plt.axis("off")
        plt.title("Estimated map", size=13)
    plt.tight_layout()
    plt.show()











