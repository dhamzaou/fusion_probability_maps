#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from scipy.special import digamma, polygamma, loggamma, softmax
from scipy import optimize
from sklearn.cluster import KMeans

def inv(x):

    eigvals, eigvecs = np.linalg.eigh(x)
    eigvals = eigvals ** (-1)
    eigvals = np.vectorize(np.diag, signature='(n)->(n,n)')(eigvals)
    transp_eigvecs = np.transpose(eigvecs, axes=(0, 2, 1))
    result = np.matmul(eigvecs, eigvals)
    result = np.matmul(result, transp_eigvecs)

    return result

class RobustMixtureFusionProbabilityMaps():

    def __init__(self, config):

        self.n_iter = config["n_iter"]
        self.eps = config["eps"]
        self.config = config
        self.tol = config["tol"]
        self.nr_n_iter = config["nr_n_iter"]
        self.nr_tol = config["nr_tol"]
        self.M = config["n_consensus"]
        self.init = config["init"]

    def initialization(self, D):
        '''
        :param D: N x P x K
        :return:
        '''

        self.N, self.P, self.K = D.shape

        mu_b = np.zeros((self.P, self.M, self.K)) # P x M x K
        precision_p = np.stack([np.stack([np.eye(self.K)*1e2 for i in range(self.M)]) for j in range(self.P)]) # P x M x K x K
        nu = np.ones(self.P)
        tau = np.random.randn(self.N, self.M, self.P) ** 2 # N x M x P
        beta = np.random.random()

        if self.init == "kmeans":
            rn = np.zeros((self.P, self.M))
            rn = np.ones((self.P, self.M)) * 0.2
            label = KMeans(n_clusters=self.M, n_init=5).fit(D[:,:,0].T).labels_
            rn[np.arange(self.P), label] = 0.8 # 1
        elif self.init == "random":
            rn = np.random.random((self.P, self.M)) * (0.6 - 0.4) + 0.4
            rn = rn / np.reshape(np.sum(rn, axis=1), (-1,1))
        else:
            raise ValueError("Initialization not recognized")

        return mu_b, precision_p, nu, tau, beta, rn

    def q_Tn(self, tau, precision_p, D, mu_b, rn):
        '''
        :param tau: N x M x P
        :param precision_p: P x M x K x K
        :param D: N x P x K
        :param mu_b: P x M x K
        :param rn: P x M
        :return: sigma_Tn, mu_Tn
        '''

        sigma_Tn = np.einsum('pm,nmp,pmkl->nmkl', rn, tau, precision_p) # N x M x K x K
        sigma_Tn = np.linalg.inv(sigma_Tn) # N x M x K x K

        Dm = np.repeat(D[np.newaxis, ...], self.M,  axis=0) # M x N x P x K
        Dm = np.transpose(Dm, (1,2,0,3)) # N x P x M x K
        D_b = Dm - mu_b # N x P x M x K
        precision_p_D_b = np.einsum('pmkl,npml->nmpk', precision_p, D_b) # N x M x P x K
        rn_tau_precision_p_D_b = np.einsum('pm,nmp,nmpk->nmk', rn, tau, precision_p_D_b) # N x M x K

        mu_Tn = np.einsum('nmkl,nml->nmk', sigma_Tn, rn_tau_precision_p_D_b) # N x M x K

        return mu_Tn, sigma_Tn

    def q_tau(self, nu, mu_Tn, sigma_Tn, precision_p, D, mu_b, sigma_b, rn):
        '''
        :param nu: P
        :param mu_Tn: N x M x K
        :param sigma_Tn: N x M x K x K
        :param precision_p: P x M x K x K
        :param D: N x P x K
        :param mu_b: P x M x K
        :param sigma_b: P x M x K x K
        :param rn: P x M
        :return: tau, a, b
        '''

        a = (self.K * rn + np.reshape(nu, (-1,1))) / 2 # P x M
        a = np.repeat(a[np.newaxis, ...], self.N, axis=0) # N x P x M
        a = np.transpose(a, (0,2,1)) # N x M x P

        Dm = np.repeat(D[np.newaxis, ...], self.M,  axis=0) # M x N x P x K
        Dm = np.transpose(Dm, (2, 1, 0, 3)) # P x N x M x K
        D_mu_Tn = Dm - mu_Tn # P x N x M x K
        D_mu_Tn_mu_b = np.transpose(D_mu_Tn, (1,0,2,3)) - mu_b # N x P x M x K

        maha = np.einsum('npmk,pmkl,npml->nmp', D_mu_Tn_mu_b, precision_p, D_mu_Tn_mu_b) # N x M x P

        precision_p_sigma_b = np.einsum('pmkl,pmlj->mpkj', precision_p, sigma_b) # M x P x K x K
        precision_p_sigma_Tn = np.einsum('pmkl,nmlj->nmpkj', precision_p, sigma_Tn) # N x M x P x K x K

        traces = np.trace(precision_p_sigma_b, axis1=2, axis2=3) + \
                 np.trace(precision_p_sigma_Tn, axis1=3, axis2=4) # N x M x P

        maha_traces = np.einsum('pm,nmp->nmp', rn, maha+traces) # N x M x P

        b = nu / 2 + 1/2 * maha_traces # N x M x P

        tau = a/b # N x M x P

        return tau, a, b

    def q_b(self, tau, precision_p, beta, D, mu_Tn, rn):
        '''
        :param tau: N x M x P
        :param precision_p: P x M x K x K
        :param beta: scalar
        :param D: N x P x K
        :param mu_Tn: N x M x K
        :param rn: P x M
        :return: mu_b, sigma_b
        '''

        sigma_b = np.einsum('pm,nmp,pmkl->pmkl', rn, tau, precision_p) + beta*np.eye(self.K, self.K) # P x M x K x K
        sigma_b = np.linalg.inv(sigma_b)

        Dm = np.repeat(D[np.newaxis, ...], self.M,  axis=0) # M x N x P x K
        Dm = np.transpose(Dm, (2, 1, 0, 3))  # P x N x M x K
        D_mu_Tn = Dm - mu_Tn  # P x N x M x K

        tau_D_mu_Tn = np.einsum('nmp,pm,pnmk->pmk', tau, rn, D_mu_Tn) # P x M x K
        precision_p_tau_D_mu_Tn = np.einsum('pmkl,pml->pmk', precision_p, tau_D_mu_Tn) # P x M x K
        mu_b = np.einsum('pmkl,pml->pmk', sigma_b, precision_p_tau_D_mu_Tn)  # P x M x K

        return mu_b, sigma_b

    def q_beta(self, sigma_b, mu_b):
        '''
        :param sigma_b: P x M x K x K
        :param mu_b: P x M x K
        :return: beta
        '''

        beta = self.P * self.M * self.K / np.sum(np.trace(sigma_b, axis1=2, axis2=3) + np.sum(mu_b**2, axis=2))

        return beta

    def q_precision_p(self, D, mu_Tn, mu_b, sigma_Tn, sigma_b, tau, rn):
        '''
        :param D: N x P x K
        :param mu_Tn: N x M x K
        :param mu_b: P x M x K
        :param sigma_Tn: N x M x K x K
        :param sigma_b: P x M x K x K
        :param tau: N x M x P
        :param rn: P x M
        :return: precision_p, sigma_p
        '''

        Dm = np.repeat(D[np.newaxis, ...], self.M,  axis=0) # M x N x P x K
        Dm = np.transpose(Dm, (2, 1, 0, 3))  # P x N x M x K
        D_mu_Tn = Dm - mu_Tn  # P x N x M x K
        D_mu_Tn = np.transpose(D_mu_Tn, (1, 0, 2, 3))  # N x P x M x K
        D_mu_Tn_mu_b = D_mu_Tn - mu_b  # N x P x M x K

        m = np.einsum('pm,npmk,nmp,npml->pmkl', rn, D_mu_Tn_mu_b, tau, D_mu_Tn_mu_b)  # P x M x K x K
        rn_tau_sigma_Tn = np.einsum('pm,nmp,nmkl->pmkl', rn, tau, sigma_Tn) # P x M x K x K
        rn_tau_sigma_b = np.einsum('pm,nmp,pmkl->pmkl', rn, tau, sigma_b)  # P x M x K x K
        sigma_p = (m + rn_tau_sigma_Tn + rn_tau_sigma_b) / self.N  + np.eye(self.K) * self.eps # P x M x K x K

        precision_p = np.linalg.inv(sigma_p)

        return precision_p, sigma_p

    def q_z(self, tau, a, b, precision_p, D, mu_Tn, sigma_Tn, mu_b, sigma_b, pi):
        '''
        :param tau: N x M x P
        :param a: N x M x P
        :param b: N x M x P
        :param precision_p: P x M x K x K
        :param D: N x P x K
        :param mu_Tn: N x M x K
        :param sigma_Tn: N x M x K x K
        :param mu_b: P x M x K
        :param sigma_b: P x M x K x K
        :param pi: M
        :return: rn
        '''

        _, log_det_precision_p = np.linalg.slogdet(precision_p)

        Dm = np.repeat(D[np.newaxis, ...], self.M,  axis=0)  # M x N x P x K
        Dm = np.transpose(Dm, (2, 1, 0, 3))  # P x N x M x K
        D_mu_Tn = Dm - mu_Tn  # P x N x M x K
        D_mu_Tn_mu_b = np.transpose(D_mu_Tn, (1, 0, 2, 3)) - mu_b  # N x P x M x K

        maha = np.einsum('npmk,nmp,pmkl,npml->pm', D_mu_Tn_mu_b, tau, precision_p, D_mu_Tn_mu_b)  # P x M

        precision_p_sigma_b = np.einsum('pmkl,pmlj->pmkj', precision_p, sigma_b)  # P x M x K x K
        precision_p_sigma_Tn = np.einsum('pmkl,nmlj->npmkj', precision_p, sigma_Tn)  # N x P x M x K x K

        traces = np.trace(precision_p_sigma_b, axis1=2, axis2=3) + \
                 np.trace(precision_p_sigma_Tn, axis1=3, axis2=4)  # N x P x M
        tau_traces = np.einsum('nmp,npm->pm', tau, traces) # P x M

        rho = np.log(pi) - self.K/2*np.log(2*np.pi) + self.K/2 * \
              np.sum((digamma(a) - np.log(b)), axis=0).T + \
              1/2 * np.sum(log_det_precision_p) - 1/2*maha - 1/2*tau_traces # P x M

        rn = softmax(rho, axis=1) # P x M

        return rn

    def q_pi(self, rn):
        '''
        :param rn: P x M
        :return: pi
        '''

        pi = np.sum(rn, axis=0) / self.P

        return pi

    def q_nu(self, tau, a, nu):
        '''
        :param tau: N x M x P
        :param a: N x M x P
        :param nu: P
        :return: nu
        '''

        # Calculate the constant part of the equation to calculate the
        # new degrees of freedom
        constant_part = 1.0 \
                        + (np.log(tau) - tau)  \
                        + digamma(a) \
                        - np.log(a)
        constant_part = np.sum(np.sum(constant_part, axis=1), axis=0) / (self.N * self.M)  # Summation over M then N

        # Solve the equation numerically using Newton-Raphson
        nu_new = np.empty_like(nu)
        for c in range(self.P):
            def func(x):
                return np.log(x / 2.0) \
                       - digamma(x / 2.0) \
                       + constant_part[c]

            def fprime(x):
                return 1.0 / x \
                       - polygamma(1, x / 2.0) / 2.0

            def fprime2(x):
                return - 1.0 / (x * x) \
                       - polygamma(2, x / 2.0) / 4.0

            nu_new[c] = optimize.newton(
                func, nu[c], fprime, args=(), tol=self.nr_tol,
                maxiter=self.nr_n_iter, fprime2=fprime2
            )
            if nu_new[c] < 0.0:
                raise ValueError('[q_nu] Error, degree of freedom smaller than one.')

        return nu_new

    def lower_bound(self, tau, a, b, precision_p, D, mu_Tn, mu_b, sigma_Tn, sigma_b, beta, nu, rn, pi):
        '''
        :param tau: N x M x P
        :param a: N x M x P
        :param b: N x M x P
        :param precision_p: P x M x K x K
        :param D: N x P x K
        :param mu_Tn: N x M x K
        :param mu_b: P x M x K
        :param sigma_Tn: N x M x K x K
        :param sigma_b: P x M x K x K
        :param beta: scalar
        :param nu: P
        :param rn: P x M
        :param pi: M
        :return: lower bound
        '''

        _, log_det_precision_p = np.linalg.slogdet(precision_p)
        _, log_det_sigma_Tn = np.linalg.slogdet(sigma_Tn)
        _, log_det_sigma_b = np.linalg.slogdet(sigma_b)

        Dm = np.repeat(D[np.newaxis, ...], self.M,  axis=0)  # M x N x P x K
        Dm = np.transpose(Dm, (2, 1, 0, 3))  # P x N x M x K
        D_mu_Tn = Dm - mu_Tn  # P x N x M x K
        D_mu_Tn_mu_b = np.transpose(D_mu_Tn, (1, 0, 2, 3)) - mu_b  # N x P x M x K

        maha = np.einsum('npmk,nmp,pmkl,npml->pm', D_mu_Tn_mu_b, tau, precision_p, D_mu_Tn_mu_b)  # P x M

        precision_p_sigma_b = np.einsum('pmkl,pmlj->pmkj', precision_p, sigma_b)  # P x M x K x K
        precision_p_sigma_Tn = np.einsum('pmkl,nmlj->npmkj', precision_p, sigma_Tn)  # N x P x M x K x K

        traces = np.trace(precision_p_sigma_b, axis1=2, axis2=3) + \
                 np.trace(precision_p_sigma_Tn, axis1=3, axis2=4)  # N x P x M
        tau_traces = np.einsum('nmp,npm->pm', tau, traces)  # P x M

        p_D = self.K/2 * np.sum(digamma(a) - np.log(b), axis=0).T + \
              1/2 * log_det_precision_p - 1/2 * maha - 1/2 * tau_traces # P x M
        p_D = np.einsum('pm,pm->pm', rn, p_D) # P x M
        p_D = np.sum(p_D)

        p_z = np.einsum('pm,m->pm', rn, np.log(pi))
        p_z = np.sum(p_z)

        p_b = self.K/2 * np.log(beta) - 1/2 * beta * np.sum(mu_b**2, axis=2) # P x M
        p_b = np.sum(p_b)

        p_tau = - loggamma(nu/2) + nu/2*np.log(nu/2) + (nu/2-1)*(digamma(a) - np.log(b)) - nu/2*tau # N x M x P
        p_tau = np.sum(p_tau)

        q_Tn = - 1/2 * log_det_sigma_Tn  # N x M
        q_Tn = np.sum(q_Tn)

        q_b = - 1/2 * log_det_sigma_b  # P
        q_b = np.sum(q_b)

        q_tau = - loggamma(a) + np.log(b) + (a-1)*digamma(a) - a # N x M x P
        q_tau = np.sum(q_tau)

        # We do a copy to handle the 0 case
        rn_copy = np.copy(rn)
        rn_copy[rn_copy == 0] = 1e-10
        rn_copy[rn_copy == 1] = 1 - 1e-10
        rn_copy = rn_copy / np.reshape(np.sum(rn_copy, axis=1), (-1, 1))
        q_z = rn_copy * np.log(rn_copy)
        q_z = np.sum(q_z)

        lb = p_D + p_tau + p_b + p_z - q_tau - q_b - q_Tn - q_z

        return lb


    def fit(self, D):
        '''
        :param D: N x P x K
        '''

        mu_b, precision_p, nu, tau, beta, rn = self.initialization(D)

        lower_bound_list = []
        converged = 0
        i = 0
        while i < self.n_iter:

            pi = self.q_pi(rn)

            mu_Tn, sigma_Tn = self.q_Tn(tau, precision_p, D, mu_b, rn)

            mu_b, sigma_b = self.q_b(tau, precision_p, beta, D, mu_Tn, rn)

            tau, a, b = self.q_tau(nu, mu_Tn, sigma_Tn, precision_p, D, mu_b, sigma_b, rn)

            beta = self.q_beta(sigma_b, mu_b)

            precision_p, sigma_p = self.q_precision_p(D, mu_Tn, mu_b, sigma_Tn, sigma_b, tau, rn)

            try:
                nu = self.q_nu(tau, a, nu)
            except:
                pass


            rn = self.q_z(tau, a, b, precision_p, D, mu_Tn, sigma_Tn, mu_b, sigma_b, pi)

            lb = self.lower_bound(tau, a, b, precision_p, D, mu_Tn, mu_b, sigma_Tn, sigma_b, beta, nu, rn, pi)

            lower_bound_list.append(lb)
            if i > 30 and lower_bound_list[-1] - lower_bound_list[-2] < self.tol:
                i = self.n_iter
                converged = 1

            i += 1

        # Check convergence
        if not converged and self.n_iter > 0:
            print("Algorithm did not converged")
        else:
            print("Algorithm converged")
        print()


        self.lower_bound_list = lower_bound_list
        self.mu_Tn = mu_Tn
        self.sigma_Tn = sigma_Tn
        self.mu_b = mu_b
        self.sigma_b = sigma_b
        self.nu = nu
        self.precision_p = precision_p
        self.sigma_p = sigma_p
        self.tau = tau
        self.beta = beta
        self.pi = pi
        self.rn = rn




if __name__ == "__main__":

    from scipy.ndimage.morphology import distance_transform_edt
    from scipy.special import expit
    import matplotlib.pyplot as plt
    from math import *


    def multivariate_t_rvs(m, S, df=np.inf, n=1):
        '''generate random variables of multivariate t distribution
        Parameters
        ----------
        m : array_like
            mean of random variable, length determines dimension of random variable
        S : array_like
            square array of covariance  matrix
        df : int or float
            degrees of freedom
        n : int
            number of observations, return random array will be (n, len(m))
        Returns
        -------
        rvs : ndarray, (n, len(m))
            each row is an independent draw of a multivariate t distributed
            random variable
        '''
        m = np.asarray(m)
        d = len(m)
        if df == np.inf:
            x = 1.
        else:
            x = np.random.chisquare(df, n) / df
        z = np.random.multivariate_normal(np.zeros(d), S, (n,))
        return m + z / np.sqrt(x)[:, None]

    # Numbers
    K = 2  # Number of classes, here background and foreground
    P = 30  # Number of raters
    M = 2 # Number of wanted consensus

    config = {
        "tol": 1e-1,
        "nr_tol": 1e-6,
        "n_iter": 300,
        "nr_n_iter": 1000,
        "eps": 1e-8,
        "n_consensus": M,
        "init": "kmeans" # random or kmeans
    }

    # Draw M images with a square at the center and compute a distance map then
    # to be converted into a probability map
    d = 30
    N = d**2
    b = 10
    imgs = []
    for i in range(M):
        img = np.zeros((d,d))
        start = np.random.randint(int(d/2)-b, int(d/2))
        starts = [2, 15]
        start = starts[i]
        img[start:start+b, start:start+b] = 1
        dm1 = distance_transform_edt(img)
        dm2 = distance_transform_edt(np.abs(img-1))
        dm = dm1 - dm2
        dm = expit(dm)
        imgs.append(dm)

    # Plot the true consensus
    plt.figure(figsize=[5*len(imgs), 5])
    for i in range(len(imgs)):
        plt.subplot(1,len(imgs),i+1)
        plt.imshow(imgs[i])
        plt.clim((0,1))
        plt.title("Distance map "+str(i+1), size=13)
        plt.axis("off")
    plt.tight_layout()
    plt.show()

    Tn = [np.concatenate([np.reshape(imgs[i], (-1,1)),
                          np.reshape(np.abs(1 - imgs[i]), (-1,1))], axis=1) for i in range(len(imgs))]
    Tn = np.stack(Tn) # M x N x K
    Tn = np.sqrt(Tn)  # M x N x K

    # Plot of true background and foreground
    label = ["Foreground", "Background"]
    count = 0
    plt.figure(figsize=[K * 5, 5*M])
    for m in range(M):
        for j in range(K):
            tmp = Tn[m,:, j]
            tmp = np.reshape(tmp, imgs[0].shape)
            count += 1
            plt.subplot(M, K, count)
            plt.imshow(tmp)
            plt.clim((0, 1))
            plt.axis("off")
            plt.title(label[j]+" "+str(m+1), size=13)
    plt.tight_layout()
    plt.show()

    # True values for the raters distributions
    pi = np.zeros(M)
    pi[0] = np.random.random() * (0.75 - 0.25) + 0.25
    pi[1] = 1 - pi[0]
    nu = np.random.random(P) * (100 - 5) + 5
    bias = np.zeros((P, M, K))
    sigma_p = np.stack([np.eye(K)*0.001 for i in range(P)]) # P x K x K

    D = np.empty((N, P, K))
    pi_sample = np.zeros(M)
    for p in range(P):
        m = np.random.choice(np.arange(M), p=pi)
        bias[p,m,:] = np.random.random(K) * (0.1 + 0.1) - 0.1
        pi_sample[m] += 1
        for i in range(N):
            sig = sigma_p[p]
            b = bias[p,m,:]
            tn = Tn[m,i,:]
            D[i,p,:] = multivariate_t_rvs(tn+b, sig, df=nu[p], n=1)
    pi_sample = pi_sample / P

    # Plot of first rater maps
    count = 0
    nrows = min(7,P)
    label = ["foreground", "background"]
    plt.figure(figsize=[K * 3, 3 * nrows])
    for i in range(nrows):
        for j in range(K):
            tmp = D[:,i,j]
            tmp = np.reshape(tmp, imgs[0].shape)
            count += 1
            plt.subplot(nrows,K,count)
            plt.imshow(tmp)
            plt.clim((0,1))
            plt.axis("off")
            plt.title("Rater "+str(i+1)+" "+label[j], size=9)
    plt.tight_layout()
    plt.show()

    fusion_model = RobustMixtureFusionProbabilityMaps(config)

    fusion_model.fit(D)

    plt.figure()
    plt.plot(fusion_model.lower_bound_list)
    plt.title("Lower bound", size=13)
    plt.xlabel("Iterations", size=10)
    plt.tight_layout()
    plt.show()

    print("Lower bound:")
    print(fusion_model.lower_bound_list)
    print()

    print("True bias:")
    for m in range(M):
        print(bias[:5,m,:])
    print()
    print("Estimated bias:")
    for m in range(M):
        print(fusion_model.mu_b[:5,m,:])
    print()

    print("True degrees of freedom:")
    print(nu)
    print()
    print("Estimated degrees of freedom:")
    print(fusion_model.nu)
    print()


    print("True pi:")
    print(pi)
    print()
    print("Sample pi:")
    print(pi_sample)
    print()
    print("Estimated pi:")
    print(fusion_model.pi)
    print()

    # Plot of estimated consensus
    consensus = fusion_model.mu_Tn
    count = 0
    plt.figure(figsize=[5*K*2,M*5])
    for m in range(fusion_model.M):
        for k in range(K):
            tmp = Tn[m,:,k]
            tmp = np.reshape(tmp, imgs[0].shape)
            count += 1
            plt.subplot(M,K*2,count)
            plt.imshow(tmp)
            plt.clim((0,1))
            plt.axis("off")
            plt.title("True map", size=13)
    for m in range(fusion_model.M):
        for k in range(K):
            tmp = consensus[:,m,k]
            tmp = np.reshape(tmp, imgs[0].shape)
            count += 1
            plt.subplot(M,K*2,count)
            plt.imshow(tmp)
            plt.clim((0,1))
            plt.axis("off")
            plt.title("Estimated map", size=13)
    plt.tight_layout()
    plt.show()









