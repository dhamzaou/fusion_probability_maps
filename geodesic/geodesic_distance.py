import geodesic.geodesic_cython  as ge_cy
import numpy as np

def geodesic_distance(img, mask, n_iter, alpha, beta, deltax=1., deltay=1., deltaz=1.):
    if img.ndim==2:
        img = np.expand_dims(img, 0)
        mask = np.expand_dims(mask, 0)
    output_c = ge_cy.cgeodesic_distance(img, mask, n_iter, alpha, beta, deltax, deltay, deltaz)
    output_c = np.squeeze(output_c)
    return output_c

if __name__=="__main__":
    import nibabel as nb
    import matplotlib.pyplot as plt

    d = 30
    N = d ** 2
    b = 5
    img = np.zeros((d, d))
    img[max(0, int(d / 2) - b):min(int(d / 2) + b, d), max(0, int(d / 2) - b):min(int(d / 2) + b, d)] = 1
    l = geodesic_distance(img, img, 50, 1.0, 1.0)
    plt.figure()
    plt.imshow(l)
    plt.show()