# cython: profile=True
import  numpy as np
cimport numpy as np
import cython


DTYPE = np.float
ctypedef np.float_t DTYPE_t
#ctypedef np.int_t t2_t

@cython.boundscheck(False)  # turn off array bounds check
@cython.wraparound(False)   # turn off negative indices ([-1,-1])
def cgeodesic_distance(np.ndarray[DTYPE_t, ndim=3] imgg, np.ndarray[DTYPE_t, ndim=3] maskk, int n_iter, float alpha, float beta, float deltax=1, float deltay=1, float deltaz=1):
    cdef np.ndarray[DTYPE_t, ndim=3] img = imgg.copy(order="C")
    cdef np.ndarray[DTYPE_t, ndim=3] mask = maskk.copy(order="C")

    cdef int n_dim = img.ndim
    cdef int dimx, dimy, dimz
    cdef Py_ssize_t  count, i, j, k, l, i_new, j_new, k_new
    cdef DTYPE_t img_value, final_distance, current_val, euclidain_distance,geodesic_dist_current, img_d
    cdef np.ndarray[DTYPE_t, ndim=3] geodesic_dist #img, mask,
    cdef np.ndarray[np.int_t, ndim=1] neighbor_x = np.array([-1, -1, -1, 0, 0, 0, +1, +1, +1, -1, -1, -1, 0])
    cdef np.ndarray[np.int_t, ndim=1] neighbor_y = np.array([-1, 0, +1, -1, 0, +1, -1, 0, +1, -1, 0, +1, -1])
    cdef np.ndarray[np.int_t, ndim=1] neighbor_z = np.array([-1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0])
    cdef np.ndarray[DTYPE_t, ndim=1] weight = np.array([deltax*deltax + deltay*deltay + deltaz*deltaz,
              deltax*deltax + deltaz*deltaz,
              deltax*deltax + deltay*deltay + deltaz*deltaz,
              deltay*deltay + deltaz*deltaz,
              deltaz*deltaz,
              deltay*deltay + deltaz*deltaz,
              deltax*deltax + deltay*deltay + deltaz*deltaz,
              deltax*deltax + deltaz*deltaz,
              deltax*deltax + deltay*deltay + deltaz*deltaz,
              deltax*deltax + deltay*deltay,
              deltax*deltax,
              deltax*deltax + deltay*deltay,
              deltay*deltay])
    #if n_dim == 2:
    #    img = np.expand_dims(imgg, 0)
    #    mask = np.expand_dims(maskk, 0)
    #else:
    #    img = imgg
    #    mask = maskk

    assert img.dtype == DTYPE and mask.dtype == DTYPE
    dimx = img.shape[0]
    dimy = img.shape[1]
    dimz = img.shape[2]


    geodesic_dist = (2**16 - 1)*(1-mask)


    for count in range(n_iter):
        for i in range(dimx):
            for j in range(dimy):
                for k in range(dimz):
                    img_value = img[i, j, k]
                    final_distance = geodesic_dist[i, j, k]
                    for l in range(13):
                        i_new = i+neighbor_x[l]
                        j_new = j+neighbor_y[l]
                        k_new = k+neighbor_z[l]

                        if (i_new >= 0) & (i_new < dimx) & (j_new >= 0) & (j_new < dimy) & (k_new >= 0) & (
                                k_new < dimz):
                            img_d = img[i_new, j_new, k_new]
                            current_val = geodesic_dist[i_new, j_new, k_new]
                            euclidian_distance = weight[l]
                            geodesic_dist_current = ((img_value - img_d)**2) / euclidian_distance
                            final_distance = min(final_distance, current_val + pow(
                                alpha * euclidian_distance + beta * geodesic_dist_current, 0.5))

                    geodesic_dist[i, j, k] = final_distance

        for i in range(dimx - 1, -1, -1):
            for j in range(dimy - 1, -1, -1):
                for k in range(dimz - 1, -1, -1):
                    img_value = img[i, j, k]
                    final_distance = geodesic_dist[i, j, k]
                    for l in range(13):
                        i_new = i-neighbor_x[l]
                        j_new = j-neighbor_y[l]
                        k_new = k-neighbor_z[l]

                        if (i_new >= 0) & (i_new < dimx) & (j_new >= 0) & (j_new < dimy) & (k_new >= 0) & (
                                k_new < dimz):
                            img_d = img[i_new, j_new, k_new]
                            current_val = geodesic_dist[i_new, j_new, k_new]
                            euclidian_distance = weight[l]
                            geodesic_dist_current = (img_value - img_d)**2 / euclidian_distance
                            final_distance = min(final_distance, current_val + pow(
                                alpha * euclidian_distance + beta * geodesic_dist_current, 0.5))

                    geodesic_dist[i, j, k] = final_distance

    return geodesic_dist