#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from scipy.special import digamma, polygamma, loggamma
from scipy import optimize

def inv(x):

    eigvals, eigvecs = np.linalg.eigh(x)
    eigvals = eigvals ** (-1)
    eigvals = np.vectorize(np.diag, signature='(n)->(n,n)')(eigvals)
    transp_eigvecs = np.transpose(eigvecs, axes=(0, 2, 1))
    result = np.matmul(eigvecs, eigvals)
    result = np.matmul(result, transp_eigvecs)

    return result

def multivariate_normal_pdf_2D(Xcoor, Ycoor, x_bf, y_bf, scale):

    var = (scale / 3) ** 2
    res = ((Xcoor - x_bf) ** 2 + (Ycoor - y_bf) ** 2).astype(np.float64)
    ind_0 = np.where(res > scale ** 2)  # Set to 0 if > to 3 standard deviation
    ind_1 = np.where(res <= scale ** 2)
    res[ind_0] = 0
    res[ind_1] = 1 / (2 * np.pi) * 1 / var * np.exp(-1 / 2 * 1 / var * res[ind_1])

    return res

def multivariate_normal_pdf_3D(Zcoor, Xcoor, Ycoor, z_bf, x_bf, y_bf, scale):

    var = (scale / 3) ** 2
    res = ((Zcoor - z_bf) ** 2 + (Xcoor - x_bf) ** 2 + (Ycoor - y_bf) ** 2).astype(np.float64)
    ind_0 = np.where(res > scale ** 2)  # Set to 0 if > to 3 standard deviation
    ind_1 = np.where(res <= scale ** 2)
    res[ind_0] = 0
    res[ind_1] = 1 / (2 * np.pi) * 1 / var * np.exp(-1 / 2 * 1 / var * res[ind_1])

    return res

class RobustFusionProbabilityMapsWithSP():

    def __init__(self, config):

        self.dim = config["dim"]
        self.n_iter = config["n_iter"]
        self.config = config
        self.tol = config["tol"]
        self.nr_n_iter = config["nr_n_iter"]
        self.nr_tol = config["nr_tol"]
        self.eps = config["eps"]

    def initialization(self, D):
        '''
        :param D: N x P x K
        :return:
        '''

        self.N, self.P, self.K = D.shape

        mu_b = np.zeros((self.P, self.K))
        precision_p = np.stack([np.eye(self.K)*1e2 for i in range(self.P)])
        beta = np.random.random()
        alpha = np.random.random()
        precision_T = np.random.random() ** 2
        mu_Tn = np.mean(D, axis=1)

        return mu_b, precision_p, beta, alpha, precision_T, mu_Tn

    def createBasisFunctions_2D(self):

        ii = np.arange(self.img_shape[0])
        jj = np.arange(self.img_shape[1])
        ii, jj = np.meshgrid(ii, jj, indexing="ij")
        ii, jj = np.reshape(ii, (-1, 1)), np.reshape(jj, (-1, 1))
        ii, jj = ii.astype(np.int), jj.astype(np.int)

        basis_functions = []
        icoord = []
        jcoord = []

        scale = self.config["scale"]
        step = self.config["step"]
        layout = self.config["layout"]
        assert(layout == "square" or layout == "quinconce")

        ii_basis_centers = np.arange(self.img_shape[0])
        jj_basis_centers = np.arange(self.img_shape[1])
        ii_basis_centers = ii_basis_centers % step
        ii_basis_centers = np.where(ii_basis_centers == 0)[0]
        jj_basis_centers = jj_basis_centers % step
        jj_basis_centers = np.where(jj_basis_centers == 0)[0]

        if layout == "quinconce":
            ii_basis_centers = ii_basis_centers[0::2]
            jj_basis_centers = jj_basis_centers[0::2]

        ii_basis_centers, jj_basis_centers = np.meshgrid(ii_basis_centers, jj_basis_centers, indexing="ij")
        ii_basis_centers, jj_basis_centers = np.reshape(ii_basis_centers, (-1, 1)), np.reshape(jj_basis_centers, (-1, 1))

        if layout == "quinconce":
            ii_sup = (ii_basis_centers + step).astype(np.int)
            jj_sup = (jj_basis_centers + step).astype(np.int)
            ind_to_remove = np.logical_or(ii_sup >= self.img_shape[0], jj_sup >= self.img_shape[1])
            ind_to_remove = np.arange(len(ii_sup))[ind_to_remove]
            ii_sup = np.delete(ii_sup, ind_to_remove)
            jj_sup = np.delete(jj_sup, ind_to_remove)
            ii_basis_centers = np.concatenate([ii_basis_centers, ii_sup])
            jj_basis_centers = np.concatenate([jj_basis_centers, jj_sup])

        # We compute the values of all pixels for each basis function
        for j in range(len(ii_basis_centers)):

            i_bf = ii_basis_centers[j]
            j_bf = jj_basis_centers[j]

            bf_values = multivariate_normal_pdf_2D(ii, jj, i_bf, j_bf, scale)
            bf_values= np.reshape(bf_values, (-1,1))

            basis_functions.append(bf_values)
            icoord.append(i_bf)
            jcoord.append(j_bf)

        basis_functions = np.concatenate(basis_functions, axis=1)

        return basis_functions, icoord, jcoord

    def createBasisFunctions_3D(self):

        zz = np.arange(self.img_shape[0])
        ii = np.arange(self.img_shape[1])
        jj = np.arange(self.img_shape[2])
        zz, ii, jj = np.meshgrid(zz, ii, jj, indexing="ij")
        zz, ii, jj = np.reshape(zz, (-1, 1)), np.reshape(ii, (-1, 1)), np.reshape(jj, (-1, 1))
        zz, ii, jj = zz.astype(np.int), ii.astype(np.int), jj.astype(np.int)

        basis_functions = []
        zcoord = []
        icoord = []
        jcoord = []

        scale = self.config["scale"]
        step = self.config["step"]
        layout = self.config["layout"]
        assert(layout == "square" or layout == "quinconce")

        zz_basis_centers = np.arange(self.img_shape[0])
        ii_basis_centers = np.arange(self.img_shape[1])
        jj_basis_centers = np.arange(self.img_shape[2])
        zz_basis_centers = zz_basis_centers % step
        zz_basis_centers = np.where(zz_basis_centers == 0)[0]
        ii_basis_centers = ii_basis_centers % step
        ii_basis_centers = np.where(ii_basis_centers == 0)[0]
        jj_basis_centers = jj_basis_centers % step
        jj_basis_centers = np.where(jj_basis_centers == 0)[0]

        if layout == "quinconce":
            zz_basis_centers = zz_basis_centers[0::2]
            ii_basis_centers = ii_basis_centers[0::2]
            jj_basis_centers = jj_basis_centers[0::2]

        zz_basis_centers, ii_basis_centers, jj_basis_centers = np.meshgrid(zz_basis_centers, ii_basis_centers, jj_basis_centers, indexing="ij")
        zz_basis_centers, ii_basis_centers, jj_basis_centers = np.reshape(zz_basis_centers, (-1, 1)), np.reshape(ii_basis_centers, (-1, 1)), np.reshape(jj_basis_centers, (-1, 1))

        if layout == "quinconce":
            zz_sup = (zz_basis_centers + step).astype(np.int)
            ii_sup = (ii_basis_centers + step).astype(np.int)
            jj_sup = (jj_basis_centers + step).astype(np.int)
            ind_to_remove = np.logical_or(ii_sup >= self.img_shape[1], jj_sup >= self.img_shape[2])
            ind_to_remove = np.logical_or(ind_to_remove, zz_sup >= self.img_shape[0])
            ind_to_remove = np.arange(len(ii_sup))[ind_to_remove]
            zz_sup = np.delete(zz_sup, ind_to_remove)
            ii_sup = np.delete(ii_sup, ind_to_remove)
            jj_sup = np.delete(jj_sup, ind_to_remove)
            zz_basis_centers = np.concatenate([zz_basis_centers, zz_sup])
            ii_basis_centers = np.concatenate([ii_basis_centers, ii_sup])
            jj_basis_centers = np.concatenate([jj_basis_centers, jj_sup])

        # We compute the values of all pixels for each basis function
        for j in range(len(ii_basis_centers)):

            z_bf = zz_basis_centers[j]
            i_bf = ii_basis_centers[j]
            j_bf = jj_basis_centers[j]

            bf_values = multivariate_normal_pdf3D(zz, ii, jj, z_bf, i_bf, j_bf, scale)
            bf_values= np.reshape(bf_values, (-1,1))

            basis_functions.append(bf_values)
            zcoord.append(z_bf)
            icoord.append(i_bf)
            jcoord.append(j_bf)

        basis_functions = np.concatenate(basis_functions, axis=1)

        return basis_functions, zcoord, icoord, jcoord

    def q_Tn(self, precision_p, D, mu_b, basis_functions, mu_w, precision_T):
        '''
        :param tau: N x P
        :param precision_p: P x K x K
        :param D: N x P x K
        :param mu_b: P x K
        :param basis_functions: N x L
        :param mu_w: L x K
        :param precision_T: scalar
        :return: sigma_Tn, mu_Tn
        '''

        tau = np.ones((self.N, self.P))
        tau_precision_p = np.einsum('np,pkl->nkl', tau, precision_p) # p is omitted in the result, summation over this axis
        sigma_Tn = tau_precision_p + np.eye(self.K) * precision_T
        sigma_Tn = inv(sigma_Tn)

        D_b = D - mu_b #  N x P x K
        precision_p_D_b = np.einsum('pkl,npl->npk', precision_p, D_b) # N x P x K
        tau_precision_p_D_b = np.einsum('np,npk->nk', tau, precision_p_D_b) # N x K

        bf_mu_w = np.einsum('nl,lk->nk', basis_functions, mu_w) # N x K
        precision_T_bf_mu_w = precision_T * bf_mu_w # N x K

        mu_Tn = np.einsum('nkl,nl->nk', sigma_Tn, tau_precision_p_D_b+precision_T_bf_mu_w) # N x K

        return mu_Tn, sigma_Tn

    def q_w(self, basis_functions, precision_T, alpha, mu_Tn):
        '''
        :param basis_functions: N x L
        :param precision_T: scalar
        :param alpha: scalar
        :param mu_Tn: N x K
        :return: mu_w, sigma_w
        '''

        bb = np.einsum('nl,nk->lk', basis_functions, basis_functions) # L x L
        precision_T_bb = precision_T * bb # L x L
        sigma_w = precision_T_bb + np.eye(self.L) * alpha # L x L
        sigma_w = inv(sigma_w[np.newaxis, ...])[0] # L x L  // It is the same matrix for all K

        bf_mu_Tn = np.einsum('nl,nk->lk', basis_functions, mu_Tn) # L x K
        precision_T_bf_mu_Tn = precision_T * bf_mu_Tn # L x K
        mu_w = np.einsum('lj,jk->lk', sigma_w, precision_T_bf_mu_Tn) # L x K

        return mu_w, sigma_w

    def q_alpha(self, sigma_w, mu_w):
        '''
        :param sigma_w: L x L
        :param mu_w: L x K
        :return: alpha
        '''

        alpha = self.L * self.K / (np.sum(mu_w**2) + np.trace(sigma_w) * self.K)

        return alpha

    def q_precision_T(self, mu_Tn, mu_w, basis_functions, sigma_Tn, sigma_w):
        '''
        :param mu_Tn: N x K
        :param mu_w: L x K
        :param basis_functions: N x L
        :param sigma_Tn: N x K x K
        :param sigma_w: L x L
        :return: precision_T, sigma_T
        '''

        bf_mu_w = np.einsum('nl,lk->nk', basis_functions, mu_w) # N x K
        mu_Tn_bf_mu_w = mu_Tn * bf_mu_w # N x K

        bf_sigma_w = np.einsum('nl,nj,jk->nlk', basis_functions, basis_functions, sigma_w)  # N x L x L
        trace = self.K * np.trace(bf_sigma_w, axis1=1, axis2=2)

        sigma_T = 1/(self.N * self.K) * (np.sum(mu_Tn_bf_mu_w) + np.sum(np.einsum('nkk', sigma_Tn)) + np.sum(trace)) + self.eps

        precision_T = 1 / sigma_T

        return precision_T, sigma_T


    def q_b(self, precision_p, beta, D, mu_Tn):
        '''
        :param tau: N x P
        :param precision_p: P x K x K
        :param beta: scalar
        :param D: N x P x K
        :param mu_Tn: N x K
        :return: mu_b, sigma_b
        '''

        tau = np.ones((self.N, self.P))
        tau_sum = np.sum(tau, axis=0) # P
        sigma_b = np.einsum('p,pkl->pkl', tau_sum, precision_p) + beta*np.eye(self.K, self.K) # P x K x K
        sigma_b = inv(sigma_b)

        D_Tn = np.transpose(D, (1,0,2)) - mu_Tn # P x N x K
        D_Tn = np.transpose(D_Tn, (1,0,2)) # N x P x K
        tau_D_Tn = np.einsum('np,npk->pk', tau, D_Tn) # P x K
        precision_p_tau_D_Tn = np.einsum('pkl,pl->pk', precision_p, tau_D_Tn) # P x K
        mu_b = np.einsum('pkl,pl->pk', sigma_b, precision_p_tau_D_Tn)  # P x K

        return mu_b, sigma_b

    def q_beta(self, sigma_b, mu_b):
        '''
        :param sigma_b: P x K x K
        :param mu_b: P x K
        :return: beta
        '''

        beta = self.P * self.K / np.sum(np.trace(sigma_b, axis1=1, axis2=2) + np.sum(mu_b**2, axis=1))

        return beta

    def q_precision_p(self, D, mu_Tn, mu_b, sigma_Tn, sigma_b):
        '''
        :param D: N x P x K
        :param mu_Tn: N x K
        :param mu_b: P x K
        :param sigma_Tn: N x K x K
        :param sigma_b: P x K x K
        :param tau: N x P
        :return: precision_p, sigma_p
        '''
        tau = np.ones((self.N, self.P))
        D_mu_Tn = np.transpose(D, (1, 0, 2)) - mu_Tn  # P x N x K
        D_mu_Tn_mu_b = np.transpose(D_mu_Tn, (1, 0, 2)) - mu_b  # N x P x K

        m = np.einsum('npk,np,npl->pkl', D_mu_Tn_mu_b, tau, D_mu_Tn_mu_b)  # P x K x K
        tau_sigma_Tn = np.einsum('np,nkl->pkl', tau, sigma_Tn) # P x K x K
        tau_sigma_b = np.einsum('np,pkl->pkl', tau, sigma_b)  # P x K x K
        sigma_p = (m + tau_sigma_Tn + tau_sigma_b) / self.N + np.eye(self.K) * self.eps # P x K x K

        precision_p = inv(sigma_p)

        return precision_p, sigma_p


    def lower_bound(self, precision_p, precision_T, D, mu_Tn, mu_b, mu_w, sigma_Tn, sigma_b, sigma_w, beta, alpha, basis_functions):
        '''
        :param precision_p: P x K x K
        :param precision_T: scalar
        :param D: N x P x K
        :param mu_Tn: N x K
        :param mu_b: P x K
        :param mu_w: L x K
        :param sigma_Tn: N x K x K
        :param sigma_b: P x K x K
        :param sigma_w: L x L
        :param beta: scalar
        :param alpha: scalar
        :param basis_functions: N x L
        :return: lower bound
        '''

        tau = np.ones((self.N, self.P))
        _, log_det_precision_p = np.linalg.slogdet(precision_p)
        _, log_det_sigma_Tn = np.linalg.slogdet(sigma_Tn)
        _, log_det_sigma_b = np.linalg.slogdet(sigma_b)
        _, log_det_sigma_w = np.linalg.slogdet(sigma_w)

        D_mu_Tn = np.transpose(D, (1, 0, 2)) - mu_Tn  # P x N x K
        D_mu_Tn_mu_b = np.transpose(D_mu_Tn, (1, 0, 2)) - mu_b  # N x P x K
        maha = np.einsum('npk,pkl,np,npl->np', D_mu_Tn_mu_b, precision_p, tau, D_mu_Tn_mu_b)  # N x P

        precision_p_sigma_b = np.einsum('pkl,plj->pkj', precision_p, sigma_b)  # P x K x K
        precision_p_sigma_Tn = np.einsum('pkl,nlj->npkj', precision_p, sigma_Tn)  # N x P x K x K
        traces = tau * (np.trace(precision_p_sigma_b, axis1=1, axis2=2) + np.trace(precision_p_sigma_Tn, axis1=2, axis2=3)) # N x P

        p_D = 1/2 * log_det_precision_p - 1/2 * maha - 1/2 * traces # N x P
        p_D = np.sum(p_D)

        mu_Tn_bf_mu_w = mu_Tn - np.einsum('nl,lk->nk', basis_functions, mu_w) # N x K
        precision_T_diag = np.eye(self.K) * precision_T # K x K
        maha = np.einsum('nk,kl,nl->n', mu_Tn_bf_mu_w, precision_T_diag, mu_Tn_bf_mu_w) # N
        precision_T_diag_sigma_Tn = np.einsum('kl,nlj->nkj', precision_T_diag, sigma_Tn) # N x K x K
        bf_sigma_w = np.einsum('nl,nj,jk->nlk', basis_functions, basis_functions, sigma_w) # N x L x L
        traces = np.trace(precision_T_diag_sigma_Tn, axis1=1, axis2=2) + precision_T * np.trace(bf_sigma_w, axis1=1, axis2=2) * self.K # N

        p_Tn = self.K/2 * np.log(precision_T) - 1/2 * maha - 1/2 * traces # N
        p_Tn = np.sum(p_Tn)

        p_w = self.L/2 * np.log(alpha) - alpha/2 * (np.trace(sigma_w) + np.sum(mu_w**2, axis=0)) # K
        p_w = np.sum(p_w)

        p_b = self.K/2 * np.log(beta) - 1/2 * beta * np.sum(mu_b**2, axis=1) # P
        p_b = np.sum(p_b)

        q_Tn = - 1/2 * log_det_sigma_Tn  # N
        q_Tn = np.sum(q_Tn)

        q_b = -1/2 * log_det_sigma_b  # P
        q_b = np.sum(q_b)

        q_w =  -self.K/2 * log_det_sigma_w

        lb = p_D + p_b + p_Tn + p_w  - q_b - q_Tn - q_w

        return lb


    def fit(self, D, img_shape):
        '''
        :param D: N x P x K
        :param: img_shape: tuple
        '''

        self.img_shape = img_shape

        # Create the basis functions
        if self.config["dim"] == 2:
            basis_functions, icoord, jcoord = self.createBasisFunctions_2D()
        else:
            basis_functions, zcoord, icoord, jcoord = self.createBasisFunctions_3D()
        self.L = basis_functions.shape[1]  # Number of basis functions

        mu_b, precision_p, beta, alpha, precision_T, mu_Tn = self.initialization(D)

        lower_bound_list = []
        converged = 0
        i = 0
        while i < self.n_iter:

            mu_w, sigma_w = self.q_w(basis_functions, precision_T, alpha, mu_Tn)

            mu_Tn, sigma_Tn = self.q_Tn(precision_p, D, mu_b, basis_functions, mu_w, precision_T)

            mu_b, sigma_b = self.q_b(precision_p, beta, D, mu_Tn)

            beta = self.q_beta(sigma_b, mu_b)

            alpha = self.q_alpha(sigma_w, mu_w)

            precision_T, sigma_T = self.q_precision_T(mu_Tn, mu_w, basis_functions, sigma_Tn, sigma_w)

            precision_p, sigma_p = self.q_precision_p(D, mu_Tn, mu_b, sigma_Tn, sigma_b)


            lb = self.lower_bound(precision_p, precision_T, D, mu_Tn, mu_b, mu_w, sigma_Tn, sigma_b, sigma_w, beta, alpha, basis_functions)

            if i > 1:
                # We check that the lower bounds is increasing
                assert lb >= lower_bound_list[-1]

            lower_bound_list.append(lb)
            if i > 1 and lower_bound_list[-1] - lower_bound_list[-2] < self.tol:
                i = self.n_iter
                converged = 1

            i += 1

        # Check convergence
        if not converged and self.n_iter > 0:
            print("Algorithm did not converged")
        else:
            print("Algorithm converged")
        print()


        self.lower_bound_list = lower_bound_list
        self.mu_Tn = mu_Tn
        self.sigma_Tn = sigma_Tn
        self.mu_b = mu_b
        self.sigma_b = sigma_b
        self.mu_w = mu_w
        self.sigma_w = sigma_w
        self.alpha = alpha
        self.precision_p = precision_p
        self.precision_T = precision_T
        self.sigma_p = sigma_p
        self.sigma_T = sigma_T
        self.beta = beta




if __name__ == "__main__":

        from scipy.ndimage.morphology import distance_transform_edt
        from scipy.special import expit
        import matplotlib.pyplot as plt
        from math import *
        from scipy.stats import multivariate_normal as m_n

        config = {
            "dim": 2,
            "scale": 16,
            "step": 8,
            "layout": "square",
            "tol": 1e-1,
            "nr_tol": 1e-6,
            "n_iter": 200,
            "nr_n_iter": 1000,
            "eps": 1e-8
        }

        # Draw an image with a square at the center and compute a distance map then
        # to be converted into a probability map
        d = 30
        N = d ** 2
        b = 5
        img = np.zeros((d, d))
        img[max(0, int(d / 2) - b):min(int(d / 2) + b, d), max(0, int(d / 2) - b):min(int(d / 2) + b, d)] = 1
        dm1 = distance_transform_edt(img)
        dm2 = distance_transform_edt(np.abs(img - 1))
        dm = dm1 - dm2
        dm = expit(dm)

        plt.figure()
        plt.imshow(dm)
        plt.clim((0, 1))
        plt.title("Distance map", size=13)
        plt.axis("off")
        plt.show()

        # Numbers
        K = 2  # Background and foreground
        P = 3  # Number of raters

        # True values
        nu = np.array([500, 2.4, 5.3])
        b = np.array([[0.5, 0.05],  # p x K
                      [-0.1, 0.001],
                      [-0.2, 0]])
        Tn = np.reshape(dm, (-1, 1))
        Tn = np.concatenate([Tn, np.abs(1 - Tn)], axis=1)
        Tn = np.sqrt(Tn)  # N x K

        # Plot of true background and foreground
        label = ["Foreground", "Background"]
        count = 0
        plt.figure(figsize=[K * 5, 5])
        for j in range(K):
            tmp = Tn[:, j]
            tmp = np.reshape(tmp, img.shape)
            count += 1
            plt.subplot(1, K, count)
            plt.imshow(tmp)
            plt.clim((0, 1))
            plt.axis("off")
            plt.title(label[j], size=13)
        plt.tight_layout()
        plt.show()

        # Setting for the rater maps
        sigma_p = np.stack([np.eye(K) * 0.002,
                            np.eye(K) * 0.005,
                            np.eye(K) * 0.01])  # P x K x K

        D = np.empty((N, P, K))
        for p in range(P):
            for i in range(N):
                sig = sigma_p[p]
                bias = b[p, :]
                tn = Tn[i, :]
                D[i, p, :] = m_n(tn + bias, sig).rvs()

        # Plot of rater maps
        count = 0
        plt.figure(figsize=[K * 5, 5 * P])
        for i in range(P):
            for j in range(K):
                tmp = D[:, i, j]
                tmp = np.reshape(tmp, img.shape)
                count += 1
                plt.subplot(P, K, count)
                plt.imshow(tmp)
                plt.clim((0, 1))
                plt.axis("off")
                plt.title("Rater " + str(i + 1), size=13)
        plt.tight_layout()
        plt.show()

        fusion_model = RobustFusionProbabilityMapsWithSP(config)

        fusion_model.fit(D, img.shape)

        plt.figure()
        plt.plot(fusion_model.lower_bound_list)
        plt.title("Lower bound", size=13)
        plt.xlabel("Iterations", size=10)
        plt.tight_layout()
        plt.show()

        print("True bias:")
        print(b)
        print()
        print("Estimated bias:")
        print(fusion_model.mu_b)
        print()

        print("True sigma_p:")
        for i in range(P):
            print(sigma_p[i])
        print()
        print("Estimated sigma_p:")
        for i in range(P):
            print(fusion_model.sigma_p[i])
        print()

        # Plot of estimated consensus
        consensus = fusion_model.mu_Tn
        count = 0
        plt.figure(figsize=[5 * 2, K * 5])
        for k in range(K):
            tmp = Tn[:, k]
            tmp = np.reshape(tmp, img.shape)
            count += 1
            plt.subplot(K, 2, count)
            plt.imshow(tmp)
            plt.clim((0, 1))
            plt.axis("off")
            plt.title("True map", size=13)
            tmp = consensus[:, k]
            tmp = np.reshape(tmp, img.shape)
            count += 1
            plt.subplot(K, 2, count)
            plt.imshow(tmp)
            plt.clim((0, 1))
            plt.axis("off")
            plt.title("Estimated map", size=13)
        plt.tight_layout()
        plt.show()












