import numpy as np
from scipy.special import digamma, polygamma, loggamma
from scipy import optimize


class SimpleFusionProbabilityMaps():

    def __init__(self, config):

        self.n_iter = config["n_iter"]
        self.config = config
        self.tol = config["tol"]
        self.nr_n_iter = config["nr_n_iter"]
        self.nr_tol = config["nr_tol"]
        self.eps = 1e-10 #In case of binary images, seems to be necessary
        self.lower_bound_list = []

    def initialization(self, D):
        '''
        :param D: N x P x K
        :return:
        '''

        self.N, self.P, self.K = D.shape

        mu_b = np.zeros((self.P, self.K))
        precision_p = np.stack([np.eye(self.K)*1e2 for i in range(self.P)])
        beta = np.random.random()
        sigma_p = np.linalg.inv(precision_p)

        return mu_b, precision_p, sigma_p, beta

    def q_Tn(self, precision_p, D, mu_b):
        '''
        :param precision_p: P x K x K
        :param D: N x P x K
        :param mu_b: P x K
        :return: sigma_Tn, mu_Tn
        '''

        sigma_Tn = np.linalg.inv(np.sum(precision_p, axis = 0)) #np.einsum('np,pkl->nkl', tau, precision_p) # p is omitted in the result, summation over this axis
        #sigma_Tn = np.linalg.inv(sigma_Tn) # N x K x K

        mu_Tn = np.dot(sigma_Tn, np.sum([np.dot(precision_p[p], \
                                                (D[:, p] - np.repeat(np.expand_dims(mu_b[p], 0), self.N, axis=0)).T) for p in
                                       range(self.P)], axis=0)).T
        #D_b = D - mu_b #  N x P x K
        #precision_p_D_b = np.einsum('pkl,npl->npk', precision_p, D_b) # N x P x K
        #mu_Tn = np.einsum('nkl,nl->nk', sigma_Tn, tau_precision_p_D_b) # N x K

        return mu_Tn, sigma_Tn

    def q_b(self, precision_p, sigma_p, beta, D, mu_Tn):
        '''
        :param tau: N x P
        :param precision_p: P x K x K
        :param beta: scalar
        :param D: N x P x K
        :param mu_Tn: N x K
        :return: mu_b, sigma_b
        '''

        #sigma_b = np.einsum('p,pkl->pkl', tau_sum, precision_p) + beta*np.eye(self.K, self.K) # P x self.K x K
        #sigma_b = np.linalg.inv(sigma_b)

        #D_Tn = np.transpose(D, (1,0,2)) - mu_Tn
        #D_Tn = np.transpose(D_Tn, (1,0,2)) # N x P x K
        #tau_D_Tn = np.einsum('np,npk->pk', tau, D_Tn) # P x K
        #precision_p_tau_D_Tn = np.einsum('pkl,pl->pk', precision_p, tau_D_Tn) # self.P x K
        #mu_b = np.einsum('pkl,pl->pk', sigma_b, precision_p_tau_D_Tn)  # self.P x K
        mu_b = np.zeros((self.P, self.K))
        sigma_b = np.zeros((self.P, self.K, self.K))
        for p in range(self.P):
            sigma_b[p] = np.linalg.inv(self.N * precision_p[p] + beta * np.eye(self.K))
            mu_b[p] = (np.dot(np.linalg.inv(self.N * np.eye(self.K) + beta * sigma_p[p]), \
                               np.sum((D[:, p] - mu_Tn), axis=0).reshape((self.K, 1)))).reshape((2))

        return mu_b, sigma_b

    def q_beta(self, sigma_b, mu_b):
        '''
        :param sigma_b: P x self.K x K
        :param mu_b: P x K
        :return: beta
        '''

        beta = self.P * self.K / np.sum(np.trace(sigma_b, axis1=1, axis2=2) + np.sum(mu_b**2, axis=1))

        return beta

    def q_precision_p(self, D, mu_Tn, mu_b, sigma_Tn, sigma_b):
        '''
        :param D: N x P x K
        :param mu_Tn: N x K
        :param mu_b: P x K
        :param sigma_Tn: N x K x K
        :param sigma_b: P x K x K
        :param tau: N x P
        :return: precision_p, sigma_p
        '''

        #D_mu_Tn = np.transpose(D, (1, 0, 2)) - mu_Tn  # P x N x K
        #D_mu_Tn_mu_b = np.transpose(D_mu_Tn, (1, 0, 2)) - mu_b  # N x P x K

        #m = np.einsum('npk,np,npl->pkl', D_mu_Tn_mu_b, tau, D_mu_Tn_mu_b)  # P x K x K
        #tau_sigma_Tn = np.einsum('np,nkl->pkl', tau, sigma_Tn) # P x K x K
        #tau_sigma_b = np.einsum('np,pkl->pkl', tau, sigma_b)  # P x K x K
        #sigma_p = (m + tau_sigma_Tn + tau_sigma_b) / self.N  # P x K x K

        sigma_p = []
        for p in range(self.P):
            # the einsum does the same computation than this part way faster
            # dkt = np.zeros((K, K))
            # for n in range(N):
            #    A = (D[p, :, n] - mu_T[:, n] - mu_bp[p]).reshape((K, 1))
            #    dkt = dkt + np.dot(A, A.T)
            # sigma_p[p] = sigma_bp[p] + sigma_T + (1/N)*dkt

            dkt1 = (D[:, p] - mu_Tn - np.repeat(np.expand_dims(mu_b[p], 0), self.N, axis=0)).T
            to_add = np.sum(np.einsum('j...,k...', dkt1, dkt1), axis=0)
            sigma_p.append(sigma_b[p] + sigma_Tn + (1 / self.N) * to_add + self.eps*np.eye(K))

        precision_p = np.linalg.inv(sigma_p)

        return precision_p, sigma_p

    def lower_bound(self, sigma_p, precision_p, D, mu_Tn, sigma_Tn, mu_b, sigma_b, beta):
        '''
        :param tau: N x P
        :param a: N x P
        :param b: N x P
        :param precision_p: P x K x K
        :param D: N x P x K
        :param mu_Tn: N x K
        :param mu_b: P x K
        :param sigma_Tn: N x K x K
        :param sigma_b: P x K x K
        :param beta: scalar
        :param nu: P
        :return: lower bound
        '''

        _, log_det_sigma_p = np.linalg.slogdet(sigma_p)
        _, log_det_sigma_Tn = np.linalg.slogdet(sigma_Tn)
        _, log_det_sigma_b = np.linalg.slogdet(sigma_b)

        lb = (self.N * self.P * self.K * np.log(beta)) / 2

        for p in range(self.P):
            lb += (-self.N * log_det_sigma_p[p]) + \
                           (-self.N * beta / 2 * (np.trace(sigma_b[p]) + np.linalg.norm(mu_b[p]) ** 2)) + \
                           (-self.N / 2 * (np.trace(np.dot(precision_p[p], sigma_b[p])) + np.trace(
                               np.dot(precision_p[p], sigma_Tn))))
            # for n in range(self.N):
            #    Ab = (D[p, :, n] - mu_T[:, n] - mu_bp[p])
            #    lower_bound += np.dot(Ab.T, Ab)
            # Same: the einsum does the work of the commented part faster
            dkt2 = (D[:,p] - mu_Tn - np.repeat(np.expand_dims(mu_b[p], 0), self.N, axis=0)).T
            to_add2 = np.einsum('ij, ij->', dkt2, dkt2)  # ij, ij-> axis=0)
            lb += to_add2
        entropy_term = 0
        for p in range(self.P):
            entropy_term += 0.5 * (self.K * np.log(2 * np.pi * np.e) + log_det_sigma_b[p])
        entropy_term += (self.N / 2) * (self.K * np.log(2 * np.pi * np.e) + log_det_sigma_Tn)
        lb += entropy_term

        return lb


    def fit(self, D):
        '''
        :param D: N x P x K
        '''

        mu_b, precision_p, sigma_p, beta = self.initialization(D)

        lower_bound = []
        converged = 0
        i = 0
        while i < self.n_iter:

            if not i%1:
                print(i)

            mu_Tn, sigma_Tn = self.q_Tn(precision_p, D, mu_b)

            mu_b, sigma_b = self.q_b(precision_p, sigma_p, beta, D, mu_Tn)

            beta = self.q_beta(sigma_b, mu_b)

            precision_p, sigma_p = self.q_precision_p(D, mu_Tn, mu_b, sigma_Tn, sigma_b)

            lb = self.lower_bound(sigma_p, precision_p, D, mu_Tn, sigma_Tn, mu_b, sigma_b, beta)

            if i > 1:
                # We check that the lower bounds is increasing
                assert lb >= lower_bound[-1]
            lower_bound.append(lb)
            if i > 1 and lower_bound[-1] - lower_bound[-2] < self.tol:
                i = self.n_iter
                converged = 1

            i += 1

        # Check convergence
        if not converged and self.n_iter > 0:
            print("Algorithm did not converged")
        else:
            print("Algorithm converged")

        self.lower_bound_list = lower_bound
        self.mu_Tn = mu_Tn
        self.sigma_Tn = sigma_Tn
        self.mu_b = mu_b
        self.sigma_b = sigma_b
        self.precision_p = precision_p
        self.sigma_p = sigma_p
        self.beta = beta

    #return self.lower_bound_list


if __name__ == "__main__":

    from scipy.ndimage.morphology import distance_transform_edt
    from scipy.special import expit
    import matplotlib.pyplot as plt
    from math import *
    from scipy.stats import multivariate_normal as m_n




    config = {
        "tol": 1e-1,
        "nr_tol": 1e-6,
        "n_iter": 200,
        "nr_n_iter": 1000,
    }

    # Draw an image with a square at the center and compute a distance map then
    # to be converted into a probability map
    d = 30
    N = d**2
    b = 5
    img = np.zeros((d,d))
    img[max(0,int(d/2)-b):min(int(d/2)+b,d), max(0,int(d/2)-b):min(int(d/2)+b,d)] = 1
    dm1 = distance_transform_edt(img)
    dm2 = distance_transform_edt(np.abs(img-1))
    dm = dm1 - dm2
    dm = expit(dm)

    plt.figure()
    plt.imshow(dm)
    plt.clim((0,1))
    plt.title("Distance map", size=13)
    plt.axis("off")
    plt.show()

    # Numbers
    K = 2 # Background and foreground
    P = 3 # Number of raters

    # True values
    nu = np.array([500, 2.4, 5.3])
    b = np.array([[0.1, 0.05],  # p x K
                  [-0.1, 0.001],
                  [0.06, -0.1]])
    Tn = np.reshape(dm, (-1,1))
    Tn = np.concatenate([Tn, np.abs(1-Tn)], axis=1)
    Tn = np.sqrt(Tn) # N x K

    # Plot of true background and foreground
    label = ["Foreground", "Background"]
    count = 0
    plt.figure(figsize=[K * 5, 5])
    for j in range(K):
        tmp = Tn[:, j]
        tmp = np.reshape(tmp, img.shape)
        count += 1
        plt.subplot(1, K, count)
        plt.imshow(tmp)
        plt.clim((0, 1))
        plt.axis("off")
        plt.title(label[j], size=13)
    plt.tight_layout()
    plt.show()

    # Setting for the rater maps
    sigma_p = np.stack([np.eye(K)*0.002,
                        np.eye(K)*0.005,
                        np.eye(K)*0.01]) # P x K x K

    D = np.empty((N, P, K))
    for p in range(P):
        for i in range(N):
            sig = sigma_p[p]
            bias = b[p,:]
            tn = Tn[i,:]
            D[i,p,:] = m_n(tn+bias, sig).rvs()

    # Plot of rater maps
    count = 0
    plt.figure(figsize=[K * 5, 5 * P])
    for i in range(P):
        for j in range(K):
            tmp = D[:,i,j]
            tmp = np.reshape(tmp, img.shape)
            count += 1
            plt.subplot(P,K,count)
            plt.imshow(tmp)
            plt.clim((0,1))
            plt.axis("off")
            plt.title("Rater "+str(i+1), size=13)
    plt.tight_layout()
    plt.show()

    fusion_model = SimpleFusionProbabilityMaps(config)

    fusion_model.fit(D)

    plt.figure()
    plt.plot(fusion_model.lower_bound_list)
    plt.title("Lower bound", size=13)
    plt.xlabel("Iterations", size=9)
    plt.tight_layout()
    plt.show()


    print("True bias:")
    print(b)
    print()
    print("Estimated bias:")
    print(fusion_model.mu_b)
    print()



    print("True sigma_p:")
    for i in range(P):
        print(sigma_p[i])
    print()
    print("Estimated sigma_p:")
    for i in range(P):
        print(fusion_model.sigma_p[i])
    print()

    # Plot of estimated consensus
    consensus = fusion_model.mu_Tn
    count = 0
    plt.figure(figsize=[5*2,K*5])
    for k in range(K):
        tmp = Tn[:,k]
        tmp = np.reshape(tmp, img.shape)
        count += 1
        plt.subplot(K,2,count)
        plt.imshow(tmp)
        plt.clim((0,1))
        plt.axis("off")
        plt.title("True map", size=13)
        tmp = consensus[:,k]
        tmp = np.reshape(tmp, img.shape)
        count += 1
        plt.subplot(K,2,count)
        plt.imshow(tmp)
        plt.clim((0,1))
        plt.axis("off")
        plt.title("Estimated map", size=13)
    plt.tight_layout()
    plt.show()



